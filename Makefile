.PHONY: install uninstall

SHELL = /bin/bash

install:
	@echo "Installing swkube..."
	@if ! test -f ${HOME}/bin/swkube; then ln -s ${PWD}/swkube ${HOME}/bin/; else echo "swkube is already installed in user bin directory. Exiting"; fi

uninstall:
	@if test -L ${HOME}/bin/swkube; then rm ${HOME}/bin/swkube; fi
